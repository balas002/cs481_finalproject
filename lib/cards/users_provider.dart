import 'package:flutter/material.dart';

import './user.dart';

class Users with ChangeNotifier {
  List<User> _users = [
    User(id: "1", name: "Rescue dogs arrived", bio: "A second shipment of dogs, all rescued from China’s meat markets, have landed at JFK Airport — their wagging tails and happy yelps saying everything about how thrilled they were to become “Americans.”",img:"assets/images/news4.jpg" ),
    User(id: "2", name: "Crufts 2021 postponed", bio: "Insurance bonus for breeders, dogs celebrated in Turkmenistan.In this time of repeatedly depressing news, how refreshing it is to read not one, not two, but three uplifting news events that affect us all directly or indirectly.",img:"assets/images/news1.jpg"),
    User(id: "3", name: "Thanksgiving pandemic", bio: "Patricia Gregg says, 'My dogs have TOTALLY made the difference for me during the COVID crisis and holidays. Not like you can just forget about feeding, poop, grooming and health issues.'",img:"assets/images/new2.jpg"),
    User(id: "4", name: "Joe Biden adopts a Dog", bio: "After a viral app called Dogsmates. Joe Biden started to love dogs and adopted a German Shephard which was a popular breed displayed in that app.",img:"assets/images/news3.jpg"),
    ];

  List<User> _usersStack = [];

  List<User> get users {
    return [..._users];
  }

  List<User> get usersStack {
    return [..._usersStack];
  }

  void loadUsersStack() {
    _usersStack = [..._users];
    notifyListeners();
  }

  void deleteFromStack(String id) {
    _users.removeWhere((user) => user.id == id);
    notifyListeners();
  }
}
