import 'package:flutter/foundation.dart';

class User {
  final String id;
  final String name;
  final String bio;
  final String img;


  User({@required this.id, @required this.name, @required this.bio, @required this.img });
}
