import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import './animated__card.dart';
import './users_provider.dart';

class CardStack extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final users = Provider.of<Users>(context).users;
    return users.isEmpty
        ? Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 55,),
                Icon(
                  Icons.watch_later,
                  size: 100,
                  color: Colors.green,
                ),
                SizedBox(height: 10,),
                Text("No more feeds!" , style: TextStyle(color: Colors.redAccent ,fontSize: 40 , fontWeight: FontWeight.bold),),
                Text("Stay tuned for more updates." , style: TextStyle(color: Colors.lightBlue ,fontSize: 20 , fontWeight: FontWeight.bold),),

              ],
            ),
          )
        : Stack(children: users.map((user) => AnimatedCard(user)).toList());
  }
}
