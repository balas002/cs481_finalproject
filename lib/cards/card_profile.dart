import 'package:flutter/material.dart';
import 'package:flutter_sqlite/cards/user.dart';
import 'package:flutter_sqlite/pages/MainScreen.dart';

class CardProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = ModalRoute.of(context).settings.arguments as User;

    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            height: 300,
            width: double.infinity,
            child: Hero(
              tag: user.id,
              child: new Image.asset(
                user.img,
                width: 600.0,
                height: 240.0,
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Text(
              user.name,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Text(
              user.bio,
              style: TextStyle(fontSize: 20),
            ),
          ),
          Container(
              margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              child:
              RaisedButton(
                onPressed: () =>
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => new MainScreen())),
                child: Text('Back'),
                textColor: Colors.white,
                color: Colors.red,
                padding: EdgeInsets.fromLTRB(12, 12, 12, 12),
              )
          ),
        ],
      ),
    );
  }
}
