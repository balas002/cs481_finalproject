import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_sqlite/Menu/main_drawer.dart';
import 'package:flutter_sqlite/bottom_nav_source/sheet_bar.dart';
import 'package:flutter_sqlite/cards/card_profile.dart';
import 'package:flutter_sqlite/cards/cards_stack.dart';

import 'package:flutter_sqlite/cards/users_provider.dart';
import 'package:flutter_sqlite/pages/services.dart';

import 'package:provider/provider.dart';
import 'package:flutter_sqlite/pages/gridlist.dart';



class MainScreen extends StatelessWidget {
  int selectedIndex = 1;
  void onChangeTab(int index) {
    selectedIndex = index;
  }
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
        value: Users(),
     child: MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          drawer: MainDrawer(),
          appBar: AppBar(
            bottom: TabBar(
              tabs: [

                Tab(icon: Icon(Icons.person)),
                Tab(icon: Icon(Icons.favorite)),
                Tab(icon: Icon(Icons.message)),
              ],
              labelColor: Colors.black.withOpacity(0.8),
              indicatorColor: Colors.black,
              unselectedLabelColor: Colors.white,

            ),
            backgroundColor: Colors.red.withOpacity(0.8),
            centerTitle: true,
            title: Text('Dogmates',style: TextStyle(fontSize: 25,fontFamily: 'Pacifico'),),
          ),
          body: TabBarView(
            children: [
              Column(
                children: <Widget>[
                  Text('Service Finder', style: TextStyle(fontSize: 25,fontFamily: 'Pacifico'),),
                  Icon(Icons.search),
                  Service(),
                ],
              ),
              Column(
                children: <Widget>[
                  Text('Latest News',style: new TextStyle(fontSize: 25, fontFamily: 'Pacifico',fontWeight: FontWeight.w700,color: Colors.black.withOpacity(0.9))),
                  CardStack(),
                 // buttonsRow(),
                ],
              ),
              Column(

                children:<Widget>[

                  Text("Know your Dog Accessories Here!!",
                    textScaleFactor: 2.0,
                    style: TextStyle(
                        fontFamily: 'Pacifico',
                        fontSize: 13,
                        color: Colors.black
                    ),),

                  Text("Long Tap on images to know names",textAlign:TextAlign.center,
                    textScaleFactor: 2.0,
                    style: TextStyle(
                        fontFamily: 'Pacifico',
                        fontSize: 8,
                        color: Colors.red
                    ),),
                  SizedBox(
                    height: 15.0,
                  ),
                  gridlist(),
                ],
              ),

              // Icon(Icons.directions_bike),
            ],
          ),
         // body:
        ),

      ),
       routes: {
         "/profile": (_) => CardProfile(),
       },
     ),
    );
  }
}

Widget buttonsRow() {
  return Container(
    margin: EdgeInsets.symmetric(vertical: 35.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
       // Text("Latest News"),
        // FloatingActionButton
        //   (
        //   heroTag: "btn1",
        //   mini: true,
        //   onPressed: () {},
        //   backgroundColor: Colors.white,
        //   child: new Icon(Icons.loop, color: Colors.yellow),
        // ),
        // new Padding(padding: new EdgeInsets.only(right: 8.0)),
        // new FloatingActionButton
        //   (
        //   heroTag: "btn2",
        //   onPressed: () {},
        //   backgroundColor: Colors.white,
        //   child: new Icon(Icons.close, color: Colors.red),
        // ),
        // new Padding(padding: new EdgeInsets.only(right: 8.0)),
        // new FloatingActionButton
        //   (
        //   heroTag: "btn3",
        //   onPressed: () {},
        //   backgroundColor: Colors.white,
        //   child: new Icon(Icons.favorite, color: Colors.green),
        // ),
        // new Padding(padding: new EdgeInsets.only(right: 8.0)),
        // new FloatingActionButton
        //   (
        //   heroTag: "btn4",
        //   mini: true,
        //   onPressed: () {},
        //   backgroundColor: Colors.white,
        //   child: new Icon(Icons.star, color: Colors.blue),
        // ),
      ],
    ),
  );
}

class MyRaisedButton extends StatefulWidget {
  final String text;

  const MyRaisedButton({Key key, @required this.text}): super(key: key);

  @override
  _MyRaisedButtonState createState() => _MyRaisedButtonState();
}

class _MyRaisedButtonState extends State<MyRaisedButton> {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      padding: EdgeInsets.only(left: 60, top: 15, right: 60, bottom: 15),
      color: Colors.black,
      onPressed: () {
        showBottomSheet(
            context: context, builder: (context) => BottomSheetWidget());
      },
      child: Text(
        widget.text,
        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.yellow),
      ),
    );
  }
}
