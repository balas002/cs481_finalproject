import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sqlite/pages/list_card1.dart';

import 'chips.dart';

class Service extends StatefulWidget {
  @override
  _Service createState() => _Service();
}

class _Service extends State<Service> {

  int _value = 0;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0),),
          Text('Choose the service for your dog!!!',textAlign:TextAlign.center, style: TextStyle(fontSize: 20,fontFamily: 'Pacifico'),),
          Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0),),
          Chips(),
          const Divider(
            color: Colors.grey,
            thickness: 2,
            indent: 2,
            endIndent: 2,
          ),
         Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0),),
         Text('These are some locations where you can find our stores and services!!!',textAlign:TextAlign.center, style: TextStyle(fontSize: 20,fontFamily: 'Pacifico'),),
          Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0),),
          Card(
            child: ListTile(
              leading: Icon(Icons.location_on,size: 40,color: Colors.redAccent,),
              title: Text('California',style: TextStyle(fontSize: 20,fontFamily: 'Pacifico'),),
              subtitle: Text('San Diego, Sacramento, San Jose, Los Angeles'),
            ),
          ),
          Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0),),
          Card(
            child: ListTile(
              leading: Icon(Icons.location_on,size: 40,color: Colors.redAccent,),
              title: Text('Texas',style: TextStyle(fontSize: 20,fontFamily: 'Pacifico'),),
              subtitle: Text('Dallas, Houston, Austin'),
            ),
          ),
          Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0),),
          Card(
            child: ListTile(
              leading: Icon(Icons.location_on,size: 40,color: Colors.redAccent,),
              title: Text('New York',style: TextStyle(fontSize: 20,fontFamily: 'Pacifico'),),
              subtitle: Text('Buffalo, Albany, Rochester'),
            ),
          ),
        ],
      ),
    );
  }
}