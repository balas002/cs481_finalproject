import 'package:flutter/material.dart';


class ListCard2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: Text('Grooming',style: TextStyle(fontFamily: 'Pacifico'),),
          backgroundColor: Colors.redAccent,
        ),
        body: ListView(
          children: <Widget>[
            Padding(padding: EdgeInsets.fromLTRB(5, 5, 5, 2),),
            Text('Find the best groomer for your pet!!',
              textAlign: TextAlign.center,
              style: TextStyle(fontFamily: 'Pacifico',color: Colors.white,fontSize: 20),),
            Padding(padding: EdgeInsets.fromLTRB(5, 2, 5, 10),),
            Card(
              child: ListTile(
                leading: Icon(Icons.contact_phone,size: 40,color: Colors.redAccent,),
                title: Text('Buffalo & Sacramento'),
                subtitle: Text('Mr. David @7605813259'),
              ),
            ),
            Padding(padding: EdgeInsets.fromLTRB(5, 10, 5, 10),),
            Card(
              child: ListTile(
                leading: Icon(Icons.contact_phone,size: 40,color: Colors.redAccent,),
                title: Text('San Jose & Dallas '),
                subtitle: Text('Ms. Julie @2136584129'),
              ),
            ),
            Padding(padding: EdgeInsets.fromLTRB(5, 10, 5, 10),),
            Card(
              child: ListTile(
                leading: Icon(Icons.contact_phone,size: 40,color: Colors.redAccent,),
                title: Text('Los Angeles & Sacramento'),
                subtitle: Text('Ms. Monie @8569135798'),
              ),
            ),
            Padding(padding: EdgeInsets.fromLTRB(5, 10, 5, 10),),
            Card(
              child: ListTile(
                leading: Icon(Icons.contact_phone,size: 40,color: Colors.redAccent,),
                title: Text('Rochester, San Diego'),
                subtitle: Text('Mr. & Mrs. Nampie @8546912365'),
              ),
            ),
            Padding(padding: EdgeInsets.fromLTRB(5, 10, 5, 10),),
            Card(
              child: ListTile(
                leading: Icon(Icons.contact_phone,size: 40,color: Colors.redAccent,),
                title: Text('Dallas & Rochester'),
                subtitle: Text('Mr. Kay @9534813576'),
              ),
            ),
            Padding(padding: EdgeInsets.fromLTRB(5, 10, 5, 10),),
            IconButton(
              padding:EdgeInsets.fromLTRB(0, 60, 0, 0) ,
              icon: Icon(Icons.arrow_back_ios,color: Colors.white,),
              onPressed: (){
                Navigator.pop(context);
              },
            )
          ],
        ),
      ),
    );
  }
}