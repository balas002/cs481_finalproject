import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sqlite/data/database-helper.dart';
import 'package:flutter_sqlite/models/user.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => new _RegisterPageState();
}

class _RegisterPageState  extends State<RegisterPage> {
  BuildContext _ctx;
  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  String _name, _username, _password;

  void _login() {
    Navigator.of(context).pushNamed("/login");
  }


  @override
  Widget build(BuildContext context) {
    _ctx = context;

    var loginBtn = new RaisedButton(
      onPressed: _submit,
      child: new Text("Register"),
      color: Colors.red,
    );
    var logBtn = new GestureDetector(
      child: Text('Existing User?Tap me!!'),
      onTap: _login,
    );


    var loginForm = new SingleChildScrollView(
        child: Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[



        new Form(
          key: formKey,
          child: new Column(
            children: <Widget>[
              new Padding(padding: EdgeInsets.only(top:40.0)),

              Container(

                height: 250,
                width:250,
                child: Image(image: AssetImage("assets/images/logindog.jpg"),
                  // fit: BoxFit.contain,
                ),
              ),

              new Text(
                "Sign Up",
                textScaleFactor: 2.0,
                style: TextStyle(
                    fontFamily: 'Pacifico',
                    fontSize: 15,
                    color: Colors.black
                ),
        ),

              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new TextFormField(
                  validator: (val)
                  {
                    if(val.isEmpty)
                      return 'Enter Name';
                  },
                  onSaved: (val) => _name = val,
                  decoration: new InputDecoration(labelText: "Name",prefixIcon:Icon(Icons.perm_identity)),
                ),
              ),
              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new TextFormField(
                  validator: (val) {
                    if (val.isEmpty)
                    {
                      return 'Enter Email';
                    }
                    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+";
                    RegExp regExp = new RegExp(p);

                    if (regExp.hasMatch(val)) {
                    // So, the email is valid
                    return null;
                    }

                    return 'This is not a valid email';
                  },

                  onSaved: (val) => _username = val,
                  decoration: new InputDecoration(labelText: "Email", prefixIcon:Icon(Icons.mail)),
                ),
              ),
              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new TextFormField(
                  validator: (val)
                  {
                    if(val.length < 6)
                      return 'Provide Minimum 6 Character';
                  },
                  onSaved: (val) => _password = val,
                  decoration: new InputDecoration(labelText: "Password",prefixIcon:Icon(Icons.lock)),
                  obscureText: true,
                ),
              )
            ],
          ),
        ),
        new Padding(
            padding: const EdgeInsets.all(10.0),
            child: loginBtn),
        logBtn,
      ],
        ),
        ),
    );

    return new Scaffold(
      key: scaffoldKey,
      body:Stack(
          children: <Widget>[
      Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
          colors: [
          Colors.pinkAccent.withOpacity(0.1),
        Colors.pinkAccent.withOpacity(0.4),
        ]
    )
    ),
    ),

    new Container(
        child: new Center(
          child: loginForm,
        ),
      ),
    ],
      ),
    );
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  void _submit(){
    final form = formKey.currentState;

    if (form.validate()) {
      setState(() {
        _isLoading = true;
        form.save();
        var user = new User(_name, _username, _password, null);
        var db = new DatabaseHelper();
        db.saveUser(user);
        _isLoading = false;
        showCupertinoDialog(
            context: context,
            builder: (context) {
              return CupertinoAlertDialog(
                title: Text(
                    'You have Successfully Registered!!'),
                actions: <Widget>[
                  CupertinoDialogAction(
                    child: Text('OK'),
                    onPressed: () {
                      Navigator.of(context).pushNamed("/login");
                    },
                  )
                ],
              );
            });
      });
    }
  }
}