import 'package:flutter/material.dart';
import 'package:flutter_sqlite/data/database-helper.dart';
import 'package:flutter_sqlite/models/user.dart';
import 'package:flutter_sqlite/pages/login/login_presenter.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> implements LoginPageContract {
  BuildContext _ctx;
  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  String _email, _password;

  LoginPagePresenter _presenter;

  _LoginPageState() {
    _presenter = new LoginPagePresenter(this);
  }

  void _register() {
    Navigator.of(context).pushNamed("/register");
  }

  void _submit() {
    final form = formKey.currentState;

    if (form.validate()) {
      setState(() {
        _isLoading = true;
        form.save();
        _presenter.doLogin(_email, _password);
      });
    }
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;
    var loginBtn = new RaisedButton(
      onPressed: _submit,
      child: new Text("Login"),
      color: Colors.red,
    );
    var registerBtn = GestureDetector(
      child: Text('New User?Tap me!!'),
      onTap: _register,
    );
    var loginForm = new  SingleChildScrollView(
      child: Container(
          child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[

        new Form(
          key: formKey,
          child: new Column(
            children: <Widget>[
              new Padding(padding: EdgeInsets.only(top:40.0)),

              Container(

                height: 250,
                width:250,
                child: Image(image: AssetImage("assets/images/logindog.jpg"),
                  // fit: BoxFit.contain,
                ),
              ),
              new Text(
                "Login",
                textScaleFactor: 2.0,
                style: TextStyle(
                    fontFamily: 'Pacifico',
                    fontSize: 20,
                    color: Colors.black
                ),
              ),
              new Padding(
                padding: const EdgeInsets.all(20.0),
                child: new TextFormField(
                    validator: (val) {
                      if (val.isEmpty) {
                        return 'Enter Email';
                      }
                    },
                  onSaved: (val) => _email = val,
                  decoration: new InputDecoration(labelText: "Email",prefixIcon:Icon(Icons.mail_outline)),
                ),
              ),
              new Padding(
                padding: const EdgeInsets.all(20.0),
                child: new TextFormField(
                  validator: (val) {
                    if (val.isEmpty) {
                      return 'Enter Password';
                    }
                  },
                  onSaved: (val) => _password = val,
                  decoration: new InputDecoration(labelText: "Password",prefixIcon:Icon(Icons.lock)),
                  obscureText: true,
                ),
              )
            ],
          ),
        ),
        new Padding(
            padding: const EdgeInsets.all(10.0),
            child: loginBtn),

        registerBtn
      ],
          ),
      ),
    );

    return new Scaffold(

      key: scaffoldKey,
      body: Stack(
          children: <Widget>[
      Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
          colors: [
          Colors.pinkAccent.withOpacity(0.1),
        Colors.pinkAccent.withOpacity(0.4),
        ]
    )
    ),
    ),
    new Container(
        child: new Center(
          child: loginForm,
        ),
      ),
    ]
      ),
    );
  }

  @override
  void onLoginError(String error) {
    // TODO: implement onLoginError
    _showSnackBar("Login not successful");
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void onLoginSuccess(User user) async {
    // TODO: implement onLoginSuccess
    if(user.username == ""){
      _showSnackBar("Login not successful");
    }else{
    _showSnackBar("Username or Password incorrect, Please Try Again!!");
    }
    setState(() {
      _isLoading = false;
    });
    if(user.flaglogged == "logged"){
      print("Logged");
      Navigator.of(context).pushNamed("/mainscreen");
    }else{
      print("Not Logged");
    }
  }
}
