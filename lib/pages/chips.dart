import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sqlite/pages/list_card2.dart';

import 'list_card1.dart';
import 'list_card3.dart';
import 'list_card4.dart';

class Chips extends StatefulWidget {
  @override
  _Chips createState() => _Chips();
}

class _Chips extends State<Chips> {
  int _value = 0;

  @override
  Widget build(BuildContext context) {
    return Wrap(
        alignment: WrapAlignment.center,
        spacing: 12.0,
        children: <Widget>[
          ChoiceChip(
            pressElevation: 0.0,
            selectedColor: Colors.transparent,
            backgroundColor: Colors.black,
            shape: StadiumBorder(side: BorderSide()),
            label: Text("Doctor",style: TextStyle(fontSize: 17,color: Colors.redAccent,fontFamily: 'Pacifico'),),
            selected: _value == 0,
            onSelected: (bool selected) {
              setState(() {
                _value = selected ? 0 : Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ListCard1()),
                );
              });
            },
          ),
          ChoiceChip(
            pressElevation: 0.0,
            selectedColor: Colors.transparent,
            backgroundColor: Colors.black,
            shape: StadiumBorder(side: BorderSide()),
            label: Text("Grooming",style: TextStyle(fontSize: 17,color: Colors.redAccent,fontFamily: 'Pacifico'),),
            selected: _value == 1,
            onSelected: (bool selected) {
              setState(() {
                _value = selected ? 1 :Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ListCard2()),
                );
              });
            },
          ),
          ChoiceChip(
            pressElevation: 0.0,
            selectedColor: Colors.transparent,
            backgroundColor: Colors.black,
            shape: StadiumBorder(side: BorderSide()),
            label: Text("Stylist",style: TextStyle(fontSize: 17,color: Colors.redAccent,fontFamily: 'Pacifico'),),
            selected: _value == 2,
            onSelected: (bool selected) {
              setState(() {
                _value = selected ? 2 : Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ListCard3()),
                );
              });
            },
          ),
          ChoiceChip(
            pressElevation: 0.0,
            selectedColor: Colors.transparent,
            backgroundColor: Colors.black,
            shape: StadiumBorder(side: BorderSide()),
            label: Text("Care Taker",style: TextStyle(fontSize: 17,color: Colors.redAccent,fontFamily: 'Pacifico'),),
            selected: _value == 3,
            onSelected: (bool selected) {
              setState(() {
                _value = selected ? 3 :Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ListCard4()),
                );
              });
            },
          ),
        ],
      );
  }
}
