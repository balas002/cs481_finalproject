import 'package:flutter/material.dart';
import 'package:flutter_sqlite/main.dart';
import 'package:flutter_sqlite/pages/MainScreen.dart';
import 'package:flutter_sqlite/pages/login/login_page.dart';
import 'dart:async';

import 'login/login_page.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 5), ()=>print('CallBack function'));
  }

  double _progress = 0;
  void startTimer() {
    new Timer.periodic(
      Duration(seconds: 1),
          (Timer timer) =>
          setState(
                () {
              if (_progress == 1) {
                timer.cancel();
                Navigator.of(context).pushNamed("/login");
               // Navigator.push(
                 //   context,
                   // MaterialPageRoute(builder: (context) => MainScreen()));
                //Navigator.push(
                //context,
                //MaterialPageRoute(builder: (context) => MyHomePage()),
                //);
              } else {
                _progress += 0.2;
              }
            },
          ),
    );
  }


  Color gradientStart = Colors.pink; //Change start gradient color here
  Color gradientEnd = Colors.redAccent.withOpacity(0.8);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            // color: Color.fromRGBO(250, 114, 104, 1),
            decoration: BoxDecoration(
              gradient:LinearGradient(colors: [gradientStart, gradientEnd],
                  begin: const FractionalOffset(0.3, 0.2),
                  end: const FractionalOffset(0.0, 0.5),
                  stops: [0.0,1.0],
                  tileMode: TileMode.clamp
              ),
            ),
          ),
          Center(
            child: Column(
                children: <Widget>[
                  Expanded(
                    //   flex: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CircleAvatar(
                          backgroundColor: Colors.black.withOpacity(0.77),
                          radius: 90.0,
                          child: Icon(
                            Icons.pets,
                            size: 100.0,
                            color: Colors.orangeAccent,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10),
                          child: Text(
                            'Welcome to Dogmates',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 32,
                              fontFamily: 'Pacifico',
                            ),
                          ),
                        ),
                        Padding(padding: EdgeInsets.fromLTRB(0, 150, 0, 0),),
                        CircularProgressIndicator(
                          strokeWidth: 5,
                          backgroundColor: Colors.cyanAccent,
                          valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),
                          value: _progress,
                        ),
                        Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0),),

                        IconButton(
                          icon: Icon(Icons.arrow_forward_ios,
                            size: 30,),
                          onPressed: () {setState(() {
                            _progress = 0;
                          });
                          startTimer();
                          },
                        ),
                        Text("Let's Get Started!!",style: TextStyle(
                            fontFamily: 'Pacifico',
                            fontSize: 15,
                            color: Colors.black
                        ),),

                      ],
                    ),
                  ),
                ],  //CircularProgressIndicator(),
          )
          )
        ],
      ),
    );
  }
}
