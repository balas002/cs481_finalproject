import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class gridlist extends StatefulWidget {
  @override
  _gridlistState createState() => _gridlistState();
}

class _gridlistState extends State<gridlist> {

  final List<String> _listItem = [
    'assets/images/dogbed.jpg',
    'assets/images/dogcollor.jpg',
    'assets/images/dogcrate.jpg',
    'assets/images/dogleash.jpg',
    'assets/images/dogbone.jpg',
    'assets/images/dogbowl.jpg',
    'assets/images/dogsweater.jpg',
    'assets/images/dogmuzzle.jpg',

  ];

  final List<String> _listItem1 = [
    'Dog Bed',
    'Dog Collar',
    'Dog Crate',
    'Metal Chain',
    'Bone Toy',
    'Food Bowl',
    'Sweater',
    'Dog Muzzle',
  ];



  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        new Container(
            height: 500,
            child:GridView.count(
              crossAxisCount: 2 ,
              children: List.generate(8,(index){
                return Container(
                  child: Tooltip(
                    message: _listItem1[index],
                    waitDuration: Duration(seconds: 0),
                    showDuration: Duration(seconds: 2),
                    child: Card(
                      color: Colors.transparent,
                      elevation: 0,
                      child: Container(
                        width: 180,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            image: DecorationImage(
                                image: AssetImage(_listItem[index]), fit: BoxFit.cover)),
                      ),
                    ),
                  ),
                );
              }),
            )
        ),
      ],
    );
  }
}
