import 'dart:ui';

import 'package:flutter/material.dart';
//import 'package:cs481_lab06/newscard.dart';
import 'dart:math';

class TypesOfBreeds extends StatefulWidget {
  @override
  _TypesOfBreeds createState() => _TypesOfBreeds();
}

class _TypesOfBreeds extends State<TypesOfBreeds> {

  static const List<String> _title = [
    'German Shepherd',
    'Labrador Retriever',
    'Golden Retriever',
    'Beagle',
    'Siberian Husky'
  ];

  static const List<String> _imgs = [
    'assets/images/german_shephard.jpg',
    'assets/images/Labrador.jpg',
    'assets/images/Golden_Retriever.jpg',
    'assets/images/Beagle.jpg',
    'assets/images/husky.jpg'
  ];
  static const List<String> _desc = [
    'The German Shepherd is a breed of medium to large-sized working dog that originated in Germany. According to the FCI, the breeds English language name is German Shepherd Dog. The breed was officially known as the "Alsatian Wolf Dog" in the UK from after the First World War until 1977 when its name was changed back to German Shepherd.',
    'The Labrador Retriever, often abbreviated to Labrador, is a breed of retriever-gun dog from the United Kingdom that was developed from imported Canadian fishing dogs. The Labrador is one of the most popular dog breeds in a number of countries in the world, particularly in the western world.',
    'The Golden Retriever is a medium-large gun dog that was bred to retrieve shot waterfowl, such as ducks and upland game birds, during hunting and shooting parties. The name "retriever" refers to the breeds ability to retrieve shot game undamaged due to their soft mouth.',
    'The beagle is a breed of small hound that is similar in appearance to the much larger foxhound. The beagle is a scent hound, developed primarily for hunting hare (beagling). It is a popular pet due to its size, good temper, and a lack of inherited health problems.',
    'The Siberian Husky is a medium-sized working dog breed. The breed belongs to the Spitz genetic family. It is recognizable by its thickly furred double coat, erect triangular ears, and distinctive markings, and is smaller than a very similar-looking dog, the Alaskan Malamute.'
  ];
  double x = 0;
  double y = 0;
  double z = 0.002;

  bool myScale = false;

  void initSate() {
    super.initState();
    for (int i = 0; i <= _imgs.length; i++) {
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
        child:Text('Our Popular Breeds'),),
        backgroundColor: Colors.black.withOpacity(0.6),
      ),
      body: ListView.builder(
        itemCount: _imgs.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) =>
        Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
         Text(_title[index],
           style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20.0,)),
            Transform(
              transform: Matrix4(
                1,0,0,0.001,
                0,1,0,0,
                0,0,1,z,
                0,0,0,1,
              )..rotateX(x)..rotateY(y)..rotateZ(z),
              alignment: FractionalOffset.center,
              child: GestureDetector(
                onPanUpdate: (details) {
                  setState(() {
                    y = y - details.delta.dx / 100;
                    x = x + details.delta.dy / 100;
                  });
                },
                child: AnimatedContainer(
                  padding: EdgeInsets.all(20),
                  width: 250,
                  height: 250,
                  duration: Duration(milliseconds: 200),
                  child:  new Image.asset(
                    _imgs[index],
                    width: 600.0,
                    height: 240.0,
                    fit: BoxFit.cover,
                  ),

                ),

              ),

            ),
          Container(
            padding: const EdgeInsets.all(5.0),
            //  color: Colors.grey[300],
              width: 220.0,
              height: 220.0,

            child: RichText(
                    textAlign: TextAlign.justify,
                  text: TextSpan(
                    style: DefaultTextStyle.of(context).style,
                    children: <TextSpan>[
                      TextSpan(text: _desc[index]),
                    ],
                  ),
                ),
          ),

            ],
          ),
        ),
    );
  }
}
