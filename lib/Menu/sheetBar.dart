import 'package:flutter/material.dart';

class BottomSheetWidget extends StatefulWidget {
  final String title;
  final String price;

  const BottomSheetWidget({Key key, @required this.title, @required this.price}) : super(key: key);

  @override
  _BottomSheetWidgetState createState() => _BottomSheetWidgetState();
}

class _BottomSheetWidgetState extends State<BottomSheetWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5, left: 15, right: 15),
      height: 135,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            height: 130,
            decoration: BoxDecoration(
                color: Colors.black54,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                      blurRadius: 5, color: Colors.black38, spreadRadius: 3)
                ]),
            child: Column(
              children: <Widget>[
                DecoratedTitle(title: widget.title),
                SizedBox(height: 20),
                DecoratedTextField(price: widget.price,)
              ],
            ),
          )
        ],
      ),
    );
  }
}

class DecoratedTitle extends StatelessWidget {
  DecoratedTitle({Key key, this.title}) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Text(
        title,
        style: TextStyle(
            fontSize: 20, color: Colors.white, fontWeight: FontWeight.w500),
      ),
    );
  }
}

class DecoratedTextField extends StatelessWidget {
  DecoratedTextField({Key key, this.price}) : super(key: key);
  final String price;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Text(
          price,
          style:
              TextStyle(fontSize: 25,fontWeight: FontWeight.w500, color: Colors.white),
        ),
        SheetButton()
      ],
    );
  }
}

class SheetButton extends StatefulWidget {
  SheetButton({Key key}) : super(key: key);

  _SheetButtonState createState() => _SheetButtonState();
}

class _SheetButtonState extends State<SheetButton> {
  bool postComment = false;
  bool success = false;

  @override
  Widget build(BuildContext context) {
    return !postComment
        ? MaterialButton(
            color: Colors.redAccent,
            onPressed: () async {
              setState(() {
                postComment = true;
              });

              await Future.delayed(Duration(seconds: 1));

              setState(() {
                success = true;
              });

              await Future.delayed(Duration(milliseconds: 500));

              Navigator.pop(context);
            },
            child: Text(
              'Buy',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          )
        : !success
            ? Container(
                padding: EdgeInsets.only(left: 25, right: 25),
                child: CircularProgressIndicator(
                  backgroundColor: Colors.grey[800],
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.redAccent),
                ),
              )
            : Icon(
                Icons.check,
                color: Colors.redAccent,
              );
  }
}
