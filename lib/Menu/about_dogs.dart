import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_sqlite/Menu/take_care.dart';
import 'package:flutter_sqlite/Menu/types_of_breeds.dart';
import 'package:flutter_sqlite/Menu/dogs_food.dart';
import 'package:flutter_sqlite/bottom_nav_source/sheet_bar.dart';
import 'package:flutter_sqlite/bottom_nav_source/tab_bar.dart';
import 'package:flutter_sqlite/bottom_nav_source/tab_icon.dart';

import 'main_drawer.dart';



class AboutDogs extends StatelessWidget {
  int selectedIndex = 1;

  //for bottom nav bar
  final iconList = [
    TabIcon(
        iconData: Icons.child_care,
        startColor: Colors.white,
        endColor: Colors.white),
    TabIcon(
        iconData: Icons.fastfood,
        startColor: Colors.white,
        endColor: Colors.white),
    TabIcon(
        iconData: Icons.person,
        startColor: Colors.white,
        endColor: Colors.white),
  ];
  void onChangeTab(int index) {
    selectedIndex = index;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          drawer: MainDrawer(),
          appBar: AppBar(
            backgroundColor: Colors.red.withOpacity(0.8),
            centerTitle: true,
            title: Text('Dogmates'),
          ),
          body: TabBarView(
            children: [
              TypesOfBreeds(),
              DogsFood(),
              DogsTakeCare(),
            ],
          ),
          bottomNavigationBar: Jumping_Bar(
            color: Colors.redAccent,
            onChangeTab: onChangeTab,
            duration: Duration(seconds: 1),
            circleGradient: RadialGradient(
              colors: [
                Colors.black.withOpacity(0.85),
                Colors.black.withOpacity(0.85),
                // Colors.grey.shade100,
                // Colors.red.shade900,
              ],
            ),
            items: iconList,
            selectedIndex: selectedIndex+2,
          ),
          // body:
        ),
      ),
    );
  }
}


Widget buttonsRow() {
  return Container(
    margin: EdgeInsets.symmetric(vertical: 48.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        FloatingActionButton
          (
          heroTag: "btn1",
          mini: true,
          onPressed: () {},
          backgroundColor: Colors.white,
          child: new Icon(Icons.loop, color: Colors.yellow),
        ),
        new Padding(padding: new EdgeInsets.only(right: 8.0)),
        new FloatingActionButton
          (
          heroTag: "btn2",
          onPressed: () {},
          backgroundColor: Colors.white,
          child: new Icon(Icons.close, color: Colors.red),
        ),
        new Padding(padding: new EdgeInsets.only(right: 8.0)),
        new FloatingActionButton
          (
          heroTag: "btn3",
          onPressed: () {},
          backgroundColor: Colors.white,
          child: new Icon(Icons.favorite, color: Colors.green),
        ),
        new Padding(padding: new EdgeInsets.only(right: 8.0)),
        new FloatingActionButton
          (
          heroTag: "btn4",
          mini: true,
          onPressed: () {},
          backgroundColor: Colors.white,
          child: new Icon(Icons.star, color: Colors.blue),
        ),
      ],
    ),
  );
}

class MyRaisedButton extends StatefulWidget {
  final String text;

  const MyRaisedButton({Key key, @required this.text}): super(key: key);

  @override
  _MyRaisedButtonState createState() => _MyRaisedButtonState();
}

class _MyRaisedButtonState extends State<MyRaisedButton> {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      padding: EdgeInsets.only(left: 60, top: 15, right: 60, bottom: 15),
      color: Colors.black,
      onPressed: () {
        showBottomSheet(
            context: context, builder: (context) => BottomSheetWidget());
      },
      child: Text(
        widget.text,
        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.yellow),
      ),
    );
  }
}
