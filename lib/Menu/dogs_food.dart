import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_sqlite/Menu/sheetBar.dart';
import 'dart:math';

class DogsFood extends StatefulWidget {
  @override
  _DogsFoodState createState() => _DogsFoodState();
}

class _DogsFoodState extends State<DogsFood> {
  List<Map<String, String>> dataRows = [
    {
      "Image":
          "https://img.chewy.com/is/image/catalog/114155_MAIN._AC_SL1500_V1595942460_.jpg",
      "Size": "4kg",
      "Price": "49.99\$",
      "Title": "Pedigree Adult"
    },
    {
      "Image":
          "https://images-na.ssl-images-amazon.com/images/I/814S6u7n%2B1L._AC_SL1500_.jpg",
      "Size": "2kg",
      "Price": "34.99\$",
      "Title": "Kibbles'nBits Original"
    },
    {
      "Image":
          "https://www.dogfoodadvisor.com/wp-content/uploads/2019/07/Blue-Buffalo-Life-Protection-Puppy-Food-Chicken-and-Brown-Rice-Formula-1-200x315.jpg",
      "Size": "1kg",
      "Price": "64.99\$",
      "Title": "Blue Life Protection Formula"
    },
    {
      "Image":
          "https://cdn.shopify.com/s/files/1/2394/1045/files/Screen_Shot_2020-04-07_at_3.44.25_PM_large.png?v=1586288680",
      "Size": "500g",
      "Price": "29.99\$",
      "Title": "Nutro Essentials"
    }
  ];

  Widget buildMyRating(double init) {
    if (init < 1) init = 1.0;
    return RatingBar.builder(
        initialRating: init,
        minRating: 1.0,
        direction: Axis.horizontal,
        itemCount: 5,
        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
        itemBuilder: (context, _) => Icon(
              Icons.star,
              color: Colors.black,
            ),
        itemSize: 15.0,
        onRatingUpdate: (rating) {
          print(rating);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 20),
        Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Colors.deepOrange,
                  Colors.deepOrange.withOpacity(0.8),
                  Colors.deepOrangeAccent.withOpacity(0.8),
                  Colors.deepOrangeAccent
                ]),
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                  color: Colors.red,
                  spreadRadius: 2,
                  blurRadius: 4,
                  offset: Offset(0, 3)),
            ],
          ),
          child: DataTable(
            showCheckboxColumn: false,
            dataRowHeight: 120,
            dividerThickness: 0,
            columnSpacing: 30.0,
            horizontalMargin: 30,
            columns: const <DataColumn>[
              DataColumn(
                label: Text(
                  'Image',
                  style: TextStyle(
                      color: Colors.white, fontStyle: FontStyle.italic),
                ),
              ),
              DataColumn(
                label: Text(
                  'Size',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.italic),
                ),
              ),
              DataColumn(
                label: Text(
                  'Price',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.italic),
                ),
              ),
              DataColumn(
                label: Text(
                  'Rate',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.italic),
                ),
              ),
            ],
            rows: dataRows
                .map(
                  ((elem) => DataRow(
                        cells: <DataCell>[
                          DataCell(Container(
                              padding: EdgeInsets.all(10),
                              child: Image.network(elem["Image"]))),
                          DataCell(Text(elem["Size"],
                              style: TextStyle(
                                  color: Colors.white, fontSize: 18))),
                          DataCell(RaisedButton(
                            onPressed: () => showBottomSheet(
                                context: context,
                                builder: (context) => BottomSheetWidget(
                                    title: elem["Title"],
                                    price: elem["Price"])),
                            color: Colors.green,
                            child: Text(elem["Price"],
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18)),
                          )),
                          DataCell(buildMyRating(
                              Random().nextInt(5).toDouble() + 2.0)),
                        ],
                      )),
                )
                .toList(),
          ),
        ),
      ],
    );
  }
}
