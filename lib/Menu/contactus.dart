import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'main_drawer.dart';


class Contact extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.redAccent.withOpacity(.8),
        title: new Text("Contact Us",
      style: new TextStyle(
          fontSize: 18.0,
          fontWeight: FontWeight.bold,
          color: Colors.white),
      ),
      ),
      drawer:MainDrawer(),
      body: ListView(
        padding: EdgeInsets.only(top: 10 ),
        children: [

          Image.asset('assets/images/contactus.gif',
            fit: BoxFit.cover,
            height:300,
            width:100,
          ),
          Padding(
            padding: EdgeInsets.only(top:55),
            child:Text("Dogmates",textAlign: TextAlign.center,
              style: new TextStyle(
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.brown),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left:50,top: 30 ),
            child:Row(
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.call,
                  color: Colors.brown,),
                Text(" (858)-457-2475",textAlign: TextAlign.center,
                  style: new TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.w500,
                      color: Colors.redAccent),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left:50,top: 30,right: 20.0) ,
            child:Row(
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.mail,
                  color: Colors.brown,),
                Text(" contactus@dogmates.com",textAlign: TextAlign.center,
                  style: new TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.w500,
                      color: Colors.redAccent),
                ),
              ],
            ),
          ),
       Padding(
         padding: EdgeInsets.only(left:50,top: 30 ),
         child:Row(
           //crossAxisAlignment: CrossAxisAlignment.center,
             children: <Widget>[
               Icon(Icons.home,
                 color: Colors.brown,size: 30,),
               Text(" 2909 San Luis Rey Rd,\nOceanside, CA 92058",textAlign: TextAlign.center,
                 style: new TextStyle(
                     fontSize: 20.0,
                     fontWeight: FontWeight.w500,
                     color: Colors.redAccent),
               ),
             ],
         ),
       ),
        ],
      ),
    );
  }
}

