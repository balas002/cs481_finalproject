import 'dart:io';

import 'package:flutter_sqlite/Menu/contactus.dart';
import 'package:flutter_sqlite/pages/MainScreen.dart';
import 'package:flutter_sqlite/Menu/about_dogs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sqlite/pages/login/login_page.dart';
import '../main.dart';
import 'EventPage.dart';

class MainDrawer extends StatelessWidget
{
  @override
  Widget build(BuildContext context) {
    return Drawer
      (
      child: new ListView(
        children: <Widget> [
       new Container(
        height: 250.0,
          child:new DrawerHeader(child: new Text('Menu',
            style:TextStyle(color: Colors.black, fontSize: 25),),

            decoration: BoxDecoration(
                color: Colors.green,

                image: DecorationImage(
                    fit: BoxFit.fitHeight,

                    image: AssetImage('assets/images/puppy_menu.jpg'))),
          ),
       ),
          new ListTile(
            leading: Icon(Icons.home, color: Colors.red.withOpacity(0.9)),
            title: new Text('Home'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => new MainScreen()));
            },
          ),

          new ListTile(
            leading: Icon(Icons.info, color: Colors.red.withOpacity(0.9)),
            title: new Text('More About Dogs'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => new AboutDogs()));
            },
          ),
          new ListTile(
            leading: Icon(Icons.account_box,color: Colors.red.withOpacity(0.9)),
            title: new Text('Event'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => new EventPage()));
            },
          ),
          new Divider(),
          new ListTile(
            leading: Icon(Icons.contact_phone, color: Colors.black,),
            title: new Text('Contact Infomation'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => new Contact()));
            },
          )
        ]
    )
    );
  }
}