import 'package:flutter/material.dart';

class DogsTakeCare extends StatefulWidget {
  @override
  _DogsTakeCareState createState() => _DogsTakeCareState();
}

class _DogsTakeCareState extends State<DogsTakeCare> {
  final List<String> imageSource = [
    "https://media.nature.com/lw800/magazine-assets/d41586-020-01430-5/d41586-020-01430-5_17977552.jpg",
    "https://www.sciencemag.org/sites/default/files/styles/article_main_large/public/dogs_1280p_0.jpg?itok=cnRk0HYq",
    "https://scx2.b-cdn.net/gfx/news/hires/2020/dog.jpg",
    "https://smartcdn.prod.postmedia.digital/calgaryherald/wp-content/uploads/2020/06/71797464-hotdogs518-w.jpg",
    "https://s.abcnews.com/images/US/160825_vod_orig_historyofdogs_16x9_992.jpg",
    "https://www.sciencemag.org/sites/default/files/styles/article_main_large/public/pearl_16x9.jpg?itok=jZ8iPy1q",
    "https://img.webmd.com/dtmcms/live/webmd/consumer_assets/site_images/article_thumbnails/slideshows/surprises_about_dogs_and_cats_slideshow/1800x1200_surprises_about_dogs_and_cats_slideshow.jpg",
    "https://www.aventri.com/hs-fs/hubfs/GettyImages-500036821.jpg?width=500&name=GettyImages-500036821.jpg"
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(bottom: 20),
          height: 350,
          child: GridView.count(
              crossAxisCount: 2,
              scrollDirection: Axis.horizontal,
              children: List.generate(8, (index) {
                return Container(
                  child: Tooltip(
                    message: "image",
                    child: Card(
                      child: Image.network(
                        imageSource[index],
                        fit: BoxFit.fitHeight,
                      ),
                      semanticContainer: true,
                      color: Colors.amber,
                    ),
                  ),
                );
              })),
        ),
        Container(
          height: 250,
          width: 410,
          decoration: BoxDecoration(
              color: Colors.deepOrangeAccent,
              borderRadius: BorderRadius.circular(20)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("How to take care of your dogs ?",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 25,
                      fontFamily: 'Pacifico')),
              SizedBox(height: 40),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(children: [
                    Icon(Icons.arrow_right, size: 35, color: Colors.white,),
                    Text(
                      "Give him nice treets",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                  ]),
                  Row(
                    children: [
                      Icon(Icons.arrow_right, size: 35, color: Colors.white,),
                      Text("Go out with him in the woods",
                          style: TextStyle(color: Colors.white, fontSize: 20)),
                    ],
                  ),
                  Row(
                    children: [
                      Icon(Icons.arrow_right, size: 35, color: Colors.white,),
                      Text("Take him in vacation with you !",
                          style: TextStyle(color: Colors.white, fontSize: 20)),
                    ],
                  )
                ],
              )
            ],
          ),
        )
      ],
    );
  }
}
