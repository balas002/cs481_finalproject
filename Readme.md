# Dogmates

## CS481 Finalproject
Subject 1: Internationalization / Localization & Dialogs
Internationalizaion: Found throughout our App 
Dialogs: Registration Page- This happens when the user has already registered.

Subject 2: Bottom Navigation & Bottom Sheet
Bottom Navigation: Found in the More about Dogs section
Bottom Sheet: Found in the second bottom tab of our App
 
Subject 3: Chips, Buttons, Data Tables, & Cards
Chips:
Buttons: Once you click on the card in the home page the sheet expands and we can see a back button there. Also found in the events page.
Data Table: Found in the second bottom tab of our App.
Cards: Home page(news section)

Subject 4: Lists, Grid Lists, & Menu
Lists:Found in the first tab of More about Dogs section.
Grid Lists:
Menu: Used side menu.

Subject 5: Snackbars, Tabs, & Tooltips
Snackbars: Login Page.
Tabs: Home page.
Tooltips:

Subject 6: Container Transform, Fade Through, & Progress Indicators
Container Transform:Found in the first tab of More about Dogs section.
Fade Through: Event Page (text)
Progress Indicators:Found in the bottom sheet.

Optional(Database)
Login and Registration page.

## Summary
Summary
